#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <sys/stat.h>
#include <sys/types.h>
#include "ViatomCheckMePro.h"

#include "webcam.h"
#include <nlohmann/json.hpp>

using namespace std;
using json = nlohmann::json;

class Barrier
{
private:
    std::mutex _mutex;
    std::condition_variable _cv;
    std::size_t _count;
    int original_count;
    int _repeat_buffer_clearing_count;
    bool _buffers_cleared;
    bool _buffers_cleared_return;
public:
    explicit Barrier(std::size_t count) : _count(count), original_count(count), _repeat_buffer_clearing_count(3), _buffers_cleared(false), _buffers_cleared_return(false) { }
    bool Wait(bool cleared = false)
    {        
        std::unique_lock<std::mutex> lock(_mutex);
        _buffers_cleared &= cleared;
        if (--_count == 0) {
            if (_buffers_cleared)
                --_repeat_buffer_clearing_count;
            _buffers_cleared_return = _buffers_cleared;
            _buffers_cleared = true;
            _count = original_count;
            _cv.notify_all();
        } else {
            _cv.wait(lock); //, [this] { return _count == 0; });
        }
        return !(_buffers_cleared_return && _repeat_buffer_clearing_count < 1);
    }
};

Barrier * barrier;
string fourcc_ext[] = {"yuyv", "nv12", "jpg"};

void run_cam(string cameraDevicePath, int cameraId, int width, int height, float fps, int gain, int cameraExposure, int whiteBalance, int fourcc, string captureDirectory, int captureTimeInSeconds, PTRVIATOMTHREADDATA pData = nullptr) {
    Webcam webcam(cameraDevicePath, width, height, fps, gain, cameraExposure, whiteBalance, fourcc);

    struct stat info;
    
    string cameraCaptureDirectory = captureDirectory + to_string(cameraId) + "/";
    
    if( stat( cameraCaptureDirectory.c_str(), &info ) != 0 )
        printf( "cannot access %s\n", cameraCaptureDirectory.c_str() );
    else 
    if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows 
    {
        printf( "%s is a directory, we are not so stupid to overwrite the data, right?\n", cameraCaptureDirectory.c_str() );
        // throw runtime_error("Target files exist...");
    }
    // else
    // {
    //     const int dir_err = mkdir(cameraCaptureDirectory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    //     if (-1 == dir_err)
    //     {
    //         printf("Error creating directory %s!\n", cameraCaptureDirectory.c_str());
    //         exit(1);
    //     }
    // }

    ofstream timestampFile;
    if (pData) {
        timestampFile.open(captureDirectory + "camera_timestamps.csv");
    }
    
    // clear buffers    
    while (true)
    {
        bool cleared = webcam.clear_buffers();        
        if (!barrier->Wait(cleared))
            break;
    }

    ofstream image;
    char image_name[300];
    auto begin = chrono::high_resolution_clock::now();
    printf("Beginning capture...\n");
    for (int i = 0; i < captureTimeInSeconds * fps; ++i)
    {
	    sprintf(image_name, "%sframe%.5d.%s", cameraCaptureDirectory.c_str(), i, fourcc_ext[fourcc].c_str());
	    image.open(image_name);
    	auto frame = webcam.frame();
    	image.write((char *) frame.data, frame.number_of_bytes);
	    image.close();
	    
	    if (pData){
	        chrono::milliseconds now = std::chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());
    		pData->recordCountMutex->lock();
    		timestampFile << now.count() << ',' << *(pData->recordCount) << '\n';
    		pData->recordCountMutex->unlock();
	    }
	    
	    barrier->Wait();
    }
    printf("Finishing capture...\n");
    auto end = chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed_secs = end - begin;
    printf("Elapsed %.2f s\n", elapsed_secs.count());
    printf("FPS: %.2f\n", ((double) captureTimeInSeconds / elapsed_secs.count()) * fps);

}

#define ARGC 2
int main(int argc, char** argv)
{
    if (argc < ARGC)
    {
		printf("\nUsage:   ./capture CONFIG_JSON\n\n");
		printf("         example:  ./grab config.json\n");
		printf("                \n\n");
		return 1;
    }

    // read a JSON file
    std::ifstream json_file(argv[1]);
    json json_config;
    json_file >> json_config;

    string captureDirectory = json_config["output_directory"];
    int captureTimeInSeconds = json_config["capture_length_in_seconds"];
    
    string uartPortname = json_config["viatom_path"];

    int width = json_config["camera_settings"]["width"];
    int height = json_config["camera_settings"]["height"];
    float fps = json_config["camera_settings"]["fps"];
    int cameraExposure = json_config["camera_settings"]["shutter_speed"];
    int fourcc = json_config["camera_settings"]["format_id"];
    int whiteBalance = json_config["camera_settings"]["white_balance"];
    int gain = json_config["camera_settings"]["gain"];

    std::vector<std::string> camera_paths = json_config["camera_paths"];

    int numberOfCameras = camera_paths.size();
    barrier = new Barrier(numberOfCameras);

    ViatomCheckMePro vcmp(uartPortname, captureDirectory); 
    PTRVIATOMTHREADDATA pData = vcmp.Record();    

    printf("[INFO] Will capture:\n\n");
    printf("[INFO]   TIME:      %d s\n", captureTimeInSeconds);
    printf("[INFO]   PATH:      %s\n\n", captureDirectory.c_str());	
    printf("[INFO]   WIDTH:     %d\n", width);
    printf("[INFO]   HEIGHT:    %d\n", height);
    printf("[INFO]   FPS:       %.2f\n", fps);
    printf("[INFO]   GAIN:      %d\n", gain);
    printf("[INFO]   EXPOSURE:  %d\n", cameraExposure);
    printf("[INFO]   FOURCC:    %s\n\n", (fourcc == 0 ? "422" : (fourcc == 1 ? "420" : (fourcc == 2 ? "MJPEG" : "422+420+MJPEG"))));

    thread ** threads = new thread*[numberOfCameras]();
    int i = 0;
    for (auto it = camera_paths.begin(); it != camera_paths.end(); ++it, ++i) { 
	    printf("[INFO]   INIT CAM %s\n", it->c_str());
    	thread * t = new thread(run_cam, *it, i, width, height, fps, gain, cameraExposure, whiteBalance, (fourcc == 3 ? i : fourcc), captureDirectory, captureTimeInSeconds, i == 0 ? pData : nullptr);
	    threads[i] = t;
    }
    printf("\n");

    printf("Ran threads, now joining...\n\n");
    for (int thread_id = 0; thread_id < numberOfCameras; ++thread_id) 
    	threads[thread_id]->join();

    delete [] threads;

    printf("Succesfully finishing...\n");
    
    return 0;
}

