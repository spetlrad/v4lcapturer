/*

   (c) 2014 Séverin Lemaignan <severin.lemaignan@epfl.ch>
   (c) 2008 Hans de Goede <hdegoede@redhat.com> for yuyv_to_rgb24

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Suite 500, Boston, MA  02110-1335  USA

 */

#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <string.h> // strerrno
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <stdexcept>

#include <linux/videodev2.h>

#include "webcam.h"

#define CLEAR(x) memset(&(x), 0, sizeof(x))

using namespace std;

static int xioctl(int fh, unsigned long int request, void *arg)
{
      int r;

      do {
            r = ioctl(fh, request, arg);
      } while (-1 == r && EINTR == errno);

      return r;
}

/*****
 * Taken from libv4l2 (in v4l-utils)
 *
 * (C) 2008 Hans de Goede <hdegoede@redhat.com>
 *
 * Released under LGPL
 */
#define CLIP(color) (unsigned char)(((color) > 0xFF) ? 0xff : (((color) < 0) ? 0 : (color)))

static void v4lconvert_yuyv_to_rgb24(const unsigned char *src, 
                                     unsigned char *dest,
                                     int width, int height, 
                                     int stride)
{
    int j;

    while (--height >= 0) {
        for (j = 0; j + 1 < width; j += 2) {
            int u = src[1];
            int v = src[3];
            int u1 = (((u - 128) << 7) +  (u - 128)) >> 6;
            int rg = (((u - 128) << 1) +  (u - 128) +
                    ((v - 128) << 2) + ((v - 128) << 1)) >> 3;
            int v1 = (((v - 128) << 1) +  (v - 128)) >> 1;

            *dest++ = CLIP(src[0] + v1);
            *dest++ = CLIP(src[0] - rg);
            *dest++ = CLIP(src[0] + u1);

            *dest++ = CLIP(src[2] + v1);
            *dest++ = CLIP(src[2] - rg);
            *dest++ = CLIP(src[2] + u1);
            src += 4;
        }
        src += stride - (width * 2);
    }
}
/*******************************************************************/


Webcam::Webcam(const string& device, int width, int height, float fps, int gain, int exposure, int white_balance, int fourcc) : 
            device(device),
            xres(width),
            yres(height),
			fps(fps),
            gain(gain),
			exposure_time(exposure),
			white_balance(white_balance),
			fourcc(fourcc)
{
    open_device();
    init_device();
    // xres and yres are set to the actual resolution provided by the cam

    // frame stored as RGB888 (ie, RGB24)
    rgb_frame.width = xres;
    rgb_frame.height = yres;
    rgb_frame.size = xres * yres * 3;
    rgb_frame.data = (unsigned char *) malloc(rgb_frame.size * sizeof(char));

    start_capturing();
}

Webcam::~Webcam()
{
      stop_capturing();
      uninit_device();
      close_device();

      free(rgb_frame.data);
}

const RGBImage& Webcam::frame(int timeout)
{
    for (;;) {
        fd_set fds;
        struct timeval tv;
        int r;

        FD_ZERO(&fds);
        FD_SET(fd, &fds);

        /* Timeout. */
        tv.tv_sec = timeout;
        tv.tv_usec = 0;

        r = select(fd + 1, &fds, NULL, NULL, &tv);

        if (-1 == r) {
            if (EINTR == errno)
                continue;
            throw runtime_error("select");
        }

        if (0 == r) {
            throw runtime_error(device + ": select timeout");
        }
        if (read_frame()) {
            return rgb_frame;
        }
        /* EAGAIN - continue select loop. */
    }

}

bool Webcam::clear_buffers()
{
    bool cleared = true;
    struct v4l2_buffer buf;
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    while (-1 != xioctl(fd, VIDIOC_DQBUF, &buf)) {
        if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
            throw runtime_error("VIDIOC_QBUF");
        cleared = false;
        // printf("[CAM%02d] Cleared buffer.\n", fd);
    }
    // printf("[CAM%02d] Finished clearing buffers.\n", fd);    

    switch (errno) {
        case EAGAIN:
            return true;

        case EIO:
            /* Could ignore EIO, see spec. */

            /* fall through */

        default:
            throw runtime_error("VIDIOC_DQBUF");
    }
    return cleared;
}

bool Webcam::read_frame()
{

    struct v4l2_buffer buf;
    unsigned int i;

    CLEAR(buf);

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
        switch (errno) {
            case EAGAIN:
                return false;

            case EIO:
                /* Could ignore EIO, see spec. */

                /* fall through */

            default:
                throw runtime_error("VIDIOC_DQBUF");
        }
    }

    assert(buf.index < n_buffers);

    /*v4lconvert_yuyv_to_rgb24((unsigned char *) buffers[buf.index].data,
                             rgb_frame.data,
                             xres,
                             yres,
                             stride);*/

    memcpy(rgb_frame.data, buffers[buf.index].data, buf.bytesused);
    rgb_frame.number_of_bytes = buf.bytesused;

    if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
        throw runtime_error("VIDIOC_QBUF");

    return true;
}

void Webcam::open_device(void)
{
      struct stat st;

      if (-1 == stat(device.c_str(), &st)) {
            throw runtime_error(device + ": cannot identify! " + to_string(errno) +  ": " + strerror(errno));
      }

      if (!S_ISCHR(st.st_mode)) {
            throw runtime_error(device + " is no device");
      }

      fd = open(device.c_str(), O_RDWR /* required */ | O_NONBLOCK, 0);

      if (-1 == fd) {
            throw runtime_error(device + ": cannot open! " + to_string(errno) + ": " + strerror(errno));
      }
}


void Webcam::init_mmap(void)
{
      struct v4l2_requestbuffers req;

      CLEAR(req);

      req.count = 4;
      req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      req.memory = V4L2_MEMORY_MMAP;

      if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
            if (EINVAL == errno) {
                  throw runtime_error(device + " does not support memory mapping");
            } else {
                  throw runtime_error("VIDIOC_REQBUFS");
            }
      }

      if (req.count < 2) {
            throw runtime_error(string("Insufficient buffer memory on ") + device);
      }

      buffers = (buffer*) calloc(req.count, sizeof(*buffers));

      if (!buffers) {
            throw runtime_error("Out of memory");
      }

      for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
            struct v4l2_buffer buf;

            CLEAR(buf);

            buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory      = V4L2_MEMORY_MMAP;
            buf.index       = n_buffers;

            if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
                  throw runtime_error("VIDIOC_QUERYBUF");

            buffers[n_buffers].size = buf.length;
            buffers[n_buffers].data =
                  mmap(NULL /* start anywhere */,
                        buf.length,
                        PROT_READ | PROT_WRITE /* required */,
                        MAP_SHARED /* recommended */,
                        fd, buf.m.offset);

            if (MAP_FAILED == buffers[n_buffers].data)
                  throw runtime_error("mmap");
      }
}

void Webcam::close_device(void)
{
      if (-1 == close(fd))
            throw runtime_error("close");

      fd = -1;
}

void Webcam::init_device(void)
{
    struct v4l2_capability cap;
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;
    struct v4l2_format fmt;
    struct v4l2_streamparm parm;
    unsigned int min;

    if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
        if (EINVAL == errno) {
            throw runtime_error(device + " is no V4L2 device");
        } else {
            throw runtime_error("VIDIOC_QUERYCAP");
        }
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        throw runtime_error(device + " is no video capture device");
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        throw runtime_error(device + " does not support streaming i/o");
    }

    /* Select video input, video standard and tune here. */

    CLEAR(fmt);

    auto pixelformat = V4L2_PIX_FMT_YUYV;
    if (fourcc == 1)
    	pixelformat = V4L2_PIX_FMT_NV12;
    if (fourcc == 2)
    	pixelformat = V4L2_PIX_FMT_MJPEG;

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (force_format) {
        fmt.fmt.pix.width       = xres;
        fmt.fmt.pix.height      = yres;
        fmt.fmt.pix.pixelformat = pixelformat;
        fmt.fmt.pix.field       = V4L2_FIELD_NONE;

        if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt))
            throw runtime_error("VIDIOC_S_FMT");

        if (fmt.fmt.pix.pixelformat != pixelformat)
            // note that libv4l2 (look for 'v4l-utils') provides helpers
            // to manage conversions
            throw runtime_error("Webcam does not support YUYV format. Support for more format need to be added!");

        /* Note VIDIOC_S_FMT may change width and height. */
        xres = fmt.fmt.pix.width;
        yres = fmt.fmt.pix.height;

        stride = fmt.fmt.pix.bytesperline;

    } else {
        /* Preserve original settings as set by v4l2-ctl for example */
        if (-1 == xioctl(fd, VIDIOC_G_FMT, &fmt))
            throw runtime_error("VIDIOC_G_FMT");
    }

    // cropping
    CLEAR(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        struct v4l2_rect rc = cropcap.defrect;
        struct v4l2_rect rb = cropcap.bounds;
        struct v4l2_fract par = cropcap.pixelaspect;
        printf("[CAM%02d] Default Cropping Rectangle(%d,%d,%d,%d), Bounds(%d,%d,%d,%d), PixelAspectRatio(%d,%d)\n", fd, rc.left, rc.top, rc.width, rc.height, rb.left, rb.top, rb.width, rb.height, par.numerator, par.denominator);

        if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop)) {
            switch (errno) {
                case EINVAL:
                    /* Cropping not supported. */
                    break;
                default:
                    /* Errors ignored. */
                    break;
            }
        }
    } else {
        /* Errors ignored. */
    }

    // set v4l2 framerate
    parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_G_PARM, &parm) < 0) {
        throw runtime_error("Couldn't query v4l fps!\n");
    }
    parm.parm.capture.timeperframe.numerator = 1000;
    parm.parm.capture.timeperframe.denominator = fps * parm.parm.capture.timeperframe.numerator;

    if (-1 == xioctl(fd, VIDIOC_S_PARM, &parm))
       throw runtime_error("Could not set the framerate :(");

    struct v4l2_fract *tf = &parm.parm.capture.timeperframe;

    if (!tf->denominator | !tf->numerator)
	    throw runtime_error("Invalid frame rate\n");

    // set_simple_ctrl(V4L2_CID_HUE_AUTO, 0, "V4L2_CID_HUE_AUTO");
    // set_simple_ctrl(V4L2_CID_CHROMA_AGC, 0, "V4L2_CID_CHROMA_AGC");
    set_simple_ctrl(V4L2_CID_POWER_LINE_FREQUENCY, 0, "V4L2_CID_POWER_LINE_FREQUENCY");
    set_simple_ctrl(V4L2_CID_BACKLIGHT_COMPENSATION, 0, "V4L2_CID_BACKLIGHT_COMPENSATION");
    set_simple_ctrl(V4L2_CID_GAIN, gain, "V4L2_CID_GAIN");
    set_simple_ctrl(V4L2_CID_AUTO_WHITE_BALANCE, 0, "V4L2_CID_AUTO_WHITE_BALANCE");
    set_simple_ctrl(V4L2_CID_WHITE_BALANCE_TEMPERATURE, white_balance, "V4L2_CID_WHITE_BALANCE_TEMPERATURE");
    set_simple_ctrl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL, "V4L2_CID_EXPOSURE_AUTO");
    set_simple_ctrl(V4L2_CID_EXPOSURE_ABSOLUTE, exposure_time, "V4L2_CID_EXPOSURE_ABSOLUTE");

    // reset_ctrl(V4L2_CID_HUE, "V4L2_CID_HUE");
    // reset_ctrl(V4L2_CID_RED_BALANCE, "V4L2_CID_RED_BALANCE");
    // reset_ctrl(V4L2_CID_BLUE_BALANCE, "V4L2_CID_BLUE_BALANCE");
    // reset_ctrl(V4L2_CID_GAMMA, "V4L2_CID_GAMMA");
    // reset_ctrl(V4L2_CID_PRIVATE_BASE, "V4L2_CID_PRIVATE_BASE"); 

    reset_ctrl(V4L2_CID_SATURATION, "V4L2_CID_SATURATION");
    reset_ctrl(V4L2_CID_BRIGHTNESS, "V4L2_CID_BRIGHTNESS");
    reset_ctrl(V4L2_CID_CONTRAST, "V4L2_CID_CONTRAST");
    // for (unsigned int i = 0; i<(unsigned int)0x1<<31; ++i)
    //     reset_ctrl(i, "V4L2_CID_PRIVATE_BASE "+std::to_string(i)); 

    init_mmap();
}

void Webcam::reset_ctrl(uint32_t id, std::string ctrl_name){
    struct v4l2_queryctrl queryctrl;
    struct v4l2_control control;

    CLEAR(queryctrl);
    queryctrl.id = id;

    if (-1 == xioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {
        if (errno != EINVAL) {
            throw runtime_error("VIDIOC_QUERYCTRL");
        } else {
            // printf("%s\n", (ctrl_name + " is not supported\n").c_str());
            throw runtime_error(ctrl_name + " is not supported\n");
        }
    } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
        throw runtime_error("V4L2_CID is not supported\n");
    } else {
        // printf("%s\n", (ctrl_name + " is supported").c_str());
        // printf("name: %s\n", queryctrl.name);
        // printf("min: %d\nmax: %d\nstep: %d\ndefault value:%d\n\n", queryctrl.minimum, queryctrl.maximum, queryctrl.step, queryctrl.default_value);

        CLEAR(control);
        control.id = id;
        control.value = queryctrl.default_value;
    
        if (-1 == ioctl(fd, VIDIOC_S_CTRL, &control)) {
            throw runtime_error(ctrl_name);            
        }
    }
}

void Webcam::set_simple_ctrl(uint32_t id, int32_t value, std::string ctrl_name){
    struct v4l2_queryctrl queryctrl;
    struct v4l2_control control;

    CLEAR(queryctrl);
    queryctrl.id = id;

    if (-1 == xioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {
        if (errno != EINVAL) {
            throw runtime_error("VIDIOC_QUERYCTRL");
        } else {
            throw runtime_error(ctrl_name + " is not supported\n");
        }
    } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
        throw runtime_error("V4L2_CID is not supported\n");
    } else {
        CLEAR(control);
        control.id = id;
        control.value = value;
    
        if (-1 == ioctl(fd, VIDIOC_S_CTRL, &control)) {
            throw runtime_error(ctrl_name);
        }
    }
}

void Webcam::uninit_device(void)
{
    unsigned int i;

    for (i = 0; i < n_buffers; ++i)
        if (-1 == munmap(buffers[i].data, buffers[i].size))
            throw runtime_error("munmap");

    free(buffers);
}

void Webcam::start_capturing(void)
{
    unsigned int i;
    enum v4l2_buf_type type;

    for (i = 0; i < n_buffers; ++i) {
        struct v4l2_buffer buf;

        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
            throw runtime_error("VIDIOC_QBUF");
    }
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
    {
	printf( "Error: %s\n", strerror( errno ) );
        throw runtime_error("VIDIOC_STREAMON");
    }
}

void Webcam::stop_capturing(void)
{
    enum v4l2_buf_type type;

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
        throw runtime_error("VIDIOC_STREAMOFF");
}


