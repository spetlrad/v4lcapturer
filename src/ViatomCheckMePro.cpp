#include "ViatomCheckMePro.h"

using namespace std;

ViatomCheckMePro::ViatomCheckMePro(string portname, string absoluteDbPath) : m_recordCount(0), m_shouldCapture(false), m_absoluteDbPath(absoluteDbPath), m_portname(portname)
{
    if (m_portname != "none")
	    configure(m_portname);
}

ViatomCheckMePro::~ViatomCheckMePro()
{ }

void ViatomCheckMePro::StopRecording()
{
	m_shouldCaptureMutex.lock();
	m_shouldCapture = false;
	m_shouldCaptureMutex.unlock();	
}

PTRVIATOMTHREADDATA ViatomCheckMePro::Record()
{
	m_shouldCapture = true;
	PTRVIATOMTHREADDATA pData = (PTRVIATOMTHREADDATA)malloc(8*sizeof(VIATOMTHREADDATA));
	pData->recordCount = &m_recordCount;
	pData->fd = &m_fd;
	pData->shouldCapture = &m_shouldCapture;
	pData->recordCountMutex = &m_recordCountMutex;
	pData->shouldCaptureMutex = &m_shouldCaptureMutex;
	pData->absoluteDbPath = &m_absoluteDbPath;
	if (m_portname != "none")
	    m_recordThread = new thread(RecordLoop, pData);
	return pData;
}

unsigned long ViatomCheckMePro::RecordLoop(PTRVIATOMTHREADDATA data)
//HANDLE* hPort, int* recordCount, bool* shouldCapture, HANDLE * recordCountMutex)
{
	ofstream rawFile;
	rawFile.open(*(data->absoluteDbPath) + "viatom-raw.csv");
	rawFile << "milliseconds, ECG, ECG HR, PPG, PPG HR, SpO2, PI" << '\n';

	char buffer[44];
	memset(buffer, 0, 44);

	bool shouldCapture = true;
	while (shouldCapture)
	{
	    int n = read(*(data->fd), buffer, 44); 
		if (n > 0)
		{
			chrono::milliseconds ms = std::chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch());
			for (int shift = 0; shift < 5; ++shift)
			{				
				rawFile << ms.count();
				rawFile << "," << get_ECG(buffer, shift);
				rawFile << "," << get_ECG_HR(buffer);
				rawFile << "," << get_PPG(buffer, shift);
				rawFile << "," << get_PPG_HR(buffer);
				rawFile << "," << get_SpO2(buffer);
				rawFile << "," << get_PI(buffer);
				rawFile << '\n';

				data->recordCountMutex->lock();
				*(data->recordCount) += 1;
				data->recordCountMutex->unlock();
			}			
		}
		else
		{
			throw runtime_error("Did not receive a single byte from Viatom!!!");
		}

		data->shouldCaptureMutex->lock();
		shouldCapture = *(data->shouldCapture);
		data->shouldCaptureMutex->unlock();
	}

	if (rawFile.is_open())
		rawFile.close();

	free(data);

	return 0;
}

int ViatomCheckMePro::SetupUart(int speed, int parity)
{
	struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (m_fd, &tty) != 0)
    {
        throw runtime_error ("error "+to_string(errno)+" from tcgetattr");
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (m_fd, TCSANOW, &tty) != 0)
    {
            throw runtime_error ("error "+to_string(errno)+" from tcsetattr");
    }
    return 1;
}

void ViatomCheckMePro::set_blocking(int should_block)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (m_fd, &tty) != 0)
    {
            runtime_error ("error "+to_string(errno)+" from tggetattr");
            return;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (m_fd, TCSANOW, &tty) != 0)
            throw runtime_error ("error "+to_string(errno)+" setting term attributes");
}

int ViatomCheckMePro::configure(string portname)
{
    m_fd = open (portname.c_str(), O_RDWR | O_NOCTTY | O_SYNC);
    if (m_fd < 0)
    {
        throw runtime_error ("error "+to_string(errno)+" opening "+portname+": "+strerror (errno));
    }

    SetupUart(B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
    set_blocking(1);                // set  blocking

	return 1;
}

int ViatomCheckMePro::CloseUart()
{
	close(m_fd);
	return 1;
}
#include <bitset>

int ViatomCheckMePro::get_ECG(char* data, int shift)
{
	int first = 0xFF000000 & (static_cast<unsigned int>(data[5 + 2 * shift]) << 24);
	int second = 0xFF0000 & (static_cast<unsigned int>(data[4 + 2 * shift]) << 16);
	return first | second;
}

int ViatomCheckMePro::get_ECG_HR(char* data)
{
	return data[14];
}

int ViatomCheckMePro::get_PPG(char* data, int shift)
{
	return data[25 + 2 * shift];
}

int ViatomCheckMePro::get_PPG_HR(char* data)
{
	return data[35];
}

int ViatomCheckMePro::get_SpO2(char* data)
{
	return data[37];
}

int ViatomCheckMePro::get_PI(char* data)
{
	return data[38];
}
