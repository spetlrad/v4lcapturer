Minimalistic C++ wrapper around V4L2
====================================

Build the demo
--------------

You need the V4L2 development package. On Debian/Ubuntu:
```
$ sudo apt update
$ sudo apt install v4l-utils
$ sudo apt install libv4l-dev
$ sudo apt install cmake
```

You also need [JSON for Modern C++][https://nlohmann.github.io/json/] library.

Then:
```
$ cmake .
$ make 
```

Example run:
```
pair viatom
./bin/grab /mnt/ 1920 1080 30.0 10 400 0 /dev/rfcomm0 /dev/video0 /dev/video2 /dev/video4 
```

