/** Small C++ wrapper around V4L example code to access the webcam
**/

#include <string>
#include <memory> // unique_ptr

struct buffer {
	  void   *data;
	  size_t  size;
};

struct RGBImage {
	  unsigned char   *data; // RGB888 <=> RGB24
	  size_t          width;
	  size_t          height;
	  size_t          size; // width * height * 3
	  size_t	      number_of_bytes;
};


class Webcam {

public:
	Webcam(const std::string& device = "/dev/video0", 
		int width = 640, 
		int height = 480,
		float fps = 30.0,
		int gain = 0,
		int exposure = 400,
		int white_balance = 5000,
		int fourcc = 0);

	~Webcam();

	/** Captures and returns a frame from the webcam.
	 *
	 * The returned object contains a field 'data' with the image data in RGB888
	 * format (ie, RGB24), as well as 'width', 'height' and 'size' (equal to
	 * width * height * 3)
	 *
	 * This call blocks until a frame is available or until the provided
	 * timeout (in seconds). 
	 *
	 * Throws a runtime_error if the timeout is reached.
	 */
	const RGBImage& frame(int timeout = 1);

	bool clear_buffers();

private:
	void init_mmap();

	void open_device();
	void close_device();

	void init_device();
	void uninit_device();    

	void start_capturing();
	void stop_capturing();

	void reset_ctrl(uint32_t id, std::string ctrl_name);
	void set_simple_ctrl(uint32_t id, int32_t value, std::string ctrl_name);

	bool read_frame();

	std::string device;
	int fd;
	int fourcc;
	float fps;
	int gain;
	int exposure_time;
	int white_balance;

	RGBImage rgb_frame;
	struct buffer          *buffers;
	unsigned int     n_buffers;

	size_t xres, yres;
	size_t stride;

	bool force_format = true;
};




