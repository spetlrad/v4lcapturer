#pragma once
#include <fstream>
#include <string>
#include <chrono>
#include <mutex>
#include <thread>
#include <cstring>
#include <errno.h>
#include <fcntl.h> 
#include <termios.h>
#include <unistd.h>

typedef struct ViatomThreadData
{
	int* recordCount;
	int* fd;
	bool * shouldCapture;
	std::mutex * recordCountMutex;
	std::mutex * shouldCaptureMutex;
	std::string* absoluteDbPath;
} VIATOMTHREADDATA, *PTRVIATOMTHREADDATA;

class ViatomCheckMePro
{
public:
	ViatomCheckMePro(std::string portname, std::string absoluteDbPath);
	void StopRecording();
	~ViatomCheckMePro();
	PTRVIATOMTHREADDATA Record();
	
	int m_fd;
	
	static int get_ECG(char* data, int shift);
	static int get_ECG_HR(char* data);
	static int get_PPG(char* data, int shift);
	static int get_PPG_HR(char* data);
	static int get_SpO2(char* data);
	static int get_PI(char* data);
	
	static unsigned long RecordLoop(PTRVIATOMTHREADDATA data);
	
	int SetupUart(int speed, int parity);
	int configure(std::string portname);
	void set_blocking(int should_block);
	int CloseUart();
	int index1 = -1, index2 = -1, index3 = -1, index4 = -1, index5 = -1, index6 = -1, index7 = -1;
	char lastError[1024], buf[100];
	std::ofstream rawFile;
	int m_recordCount;
	bool m_shouldCapture;
	std::mutex m_shouldCaptureMutex;
	std::thread * m_recordThread;
	std::mutex m_recordCountMutex;
	std::string m_absoluteDbPath;
	std::string m_portname;
};
