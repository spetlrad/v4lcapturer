#!/bin/bash

if [ "$#" -ne 3 ]; then
	echo "Usage: $0 video_device audio_card,audio_device output_path "
	echo "Example: $0 /dev/video0 0,0 /mnt/ssd/output.avi"
	echo "HINT: arecord -l && v4l2-ctl --list--devices"
	exit 1
fi

device=$1
audio_card_device="$2" 
output_path=$3 


ffmpeg -f alsa -r 16000 -i hw:"$audio_card_device" -f v4l2 -framerate 30 -video_size 1920x1080 -input_format mjpeg -i "$device" -c:v copy -acodec libmp3lame -ab 96k "$output_path" 
