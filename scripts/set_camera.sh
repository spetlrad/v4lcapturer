record () 
{
devname=$1
camid=$2
v4l2-ctl -d $devname -c gain=200
v4l2-ctl -d $devname -c exposure_auto=1
v4l2-ctl -d $devname -c exposure_absolute=400 
v4l2-ctl -d $devname -c exposure_auto_priority=0
v4l2-ctl -d $devname -c white_balance_temperature_auto=0
v4l2-ctl -d $devname -c backlight_compensation=0
v4l2-ctl -d $devname -c white_balance_temperature=2000
v4l2-ctl -d $devname -c zoom_absolute=100
v4l2-ctl -d $devname -c focus_auto=0
v4l2-ctl -d $devname -c focus_absolute=100
#ffmpeg -nostdin -f v4l2 -framerate 30 -video_size 4096x2160 -input_format mjpeg -i $devname -c:v copy tst$camid.mkv >cam$camid.out 2>&1 &
}

record /dev/video0 1
record /dev/video1 2
record /dev/video2 3
